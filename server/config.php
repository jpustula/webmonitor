<?php

$config = [

    // On production debug mode should be disabled!
    'debug' => false,

    // Server-Sent Events push interval in seconds
    'sse_push_interval' => 2,

];
