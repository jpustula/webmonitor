<?php

namespace Server\Details;

class Network
{
    private $hostname;
    private $ip;

    public function __construct()
    {
        $this->setHostname();
        $this->setIp();
    }

    private function setHostname()
    {
        $cmd = shell_exec('hostname -f');

        $this->hostname = trim($cmd);
    }

    public function getHostname()
    {
        return $this->hostname;
    }

    private function setIp()
    {
        $cmd = shell_exec("ifconfig eth1 | grep 'inet addr:' | cut -d':' -f2 | awk '{print $1}'");

        $this->ip = trim($cmd);
    }

    public function getIp()
    {
        return $this->ip;
    }
}
