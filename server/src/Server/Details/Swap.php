<?php

namespace Server\Details;

class Swap
{
    private $size;
    private $free;
    private $used;

    public function __construct()
    {
        $this->setSize();
        $this->setFree();
        $this->setUsed();
    }

    private function setSize()
    {
        $cmd = shell_exec("cat /proc/meminfo | grep 'SwapTotal:' | cut -d':' -f2");
        $cmd = trim($cmd);
        $cmd = rtrim($cmd, ' kB');

        $this->size = $cmd * 1024;
    }

    public function getSize()
    {
        return $this->size;
    }

    private function setFree()
    {
        $cmd = shell_exec("cat /proc/meminfo | grep 'SwapFree:' | cut -d':' -f2");
        $cmd = trim($cmd);
        $cmd = rtrim($cmd, ' kB');

        $this->free = $cmd * 1024;
    }

    public function getFree()
    {
        return $this->free;
    }

    private function setUsed()
    {
        $this->used = $this->size - $this->free;
    }

    public function getUsed()
    {
        return $this->used;
    }
}
