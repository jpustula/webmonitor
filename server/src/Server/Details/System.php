<?php

namespace Server\Details;

class System
{
    private $operatingSystem;
    private $uptime;

    public function __construct()
    {
        $this->setOperatingSystem();
        $this->setUptime();
    }

    private function setOperatingSystem()
    {
        $cmd = shell_exec("cat /etc/*-release | grep 'PRETTY_NAME=' | cut -d'=' -f2");
        $cmd = trim($cmd);
        $cmd = trim($cmd, '"');

        $this->operatingSystem = $cmd;
    }

    public function getOperatingSystem()
    {
        return $this->operatingSystem;
    }

    private function setUptime()
    {
        $cmd = shell_exec('uptime -p');
        $cmd = ltrim($cmd, 'up ');

        $this->uptime = trim($cmd);
    }

    public function getUptime()
    {
        return $this->uptime;
    }
}
