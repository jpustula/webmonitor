<?php

namespace Server\Details;

class Memory
{
    private $size;
    private $free;
    private $used;

    public function __construct()
    {
        $this->setSize();
        $this->setFree();
        $this->setUsed();
    }

    private function setSize()
    {
        $cmd = shell_exec("cat /proc/meminfo | grep 'MemTotal:' | cut -d':' -f2");
        $cmd = trim($cmd);
        $cmd = rtrim($cmd, ' kB');

        $this->size = $cmd * 1024;
    }

    public function getSize()
    {
        return $this->size;
    }

    private function setFree()
    {
        $cmd_memory_free = shell_exec("cat /proc/meminfo | grep 'MemFree:' | cut -d':' -f2");
        $cmd_memory_free = trim($cmd_memory_free);
        $cmd_memory_free = (int) rtrim($cmd_memory_free, ' kB');

        $cmd_memory_cached = shell_exec("cat /proc/meminfo | grep 'Cached:' | cut -d':' -f2");
        $cmd_memory_cached = trim($cmd_memory_cached);
        $cmd_memory_cached = (int) rtrim($cmd_memory_cached, ' kB');

        $cmd_memory_buffered = shell_exec("cat /proc/meminfo | grep 'Buffers:' | cut -d':' -f2");
        $cmd_memory_buffered = trim($cmd_memory_buffered);
        $cmd_memory_buffered = (int) rtrim($cmd_memory_buffered, ' kB');

        $this->free = ($cmd_memory_free + $cmd_memory_cached + $cmd_memory_buffered) * 1024;
    }

    public function getFree()
    {
        return $this->free;
    }

    private function setUsed()
    {
        $this->used = $this->size - $this->free;
    }

    public function getUsed()
    {
        return $this->used;
    }
}
