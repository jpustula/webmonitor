<?php

namespace Server\Details;

class Cpu
{
    private $load;
    private $coresNumber;

    public function __construct()
    {
        $this->setLoad();
        $this->setCoresNumber();
    }

    private function setLoad()
    {
        $this->load = sys_getloadavg();
    }

    public function getLoad()
    {
        return $this->load;
    }

    private function setCoresNumber()
    {
        $this->coresNumber = trim(shell_exec('nproc'));
    }

    public function getCoresNumber()
    {
        return $this->coresNumber;
    }
}
