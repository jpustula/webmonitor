<?php

namespace Server\Details;

class Disk
{
    private $totalSpace;
    private $freeSpace;
    private $usedSpace;

    public function __construct()
    {
        $this->setSpaceStatistics();
    }

    private function setSpaceStatistics()
    {
        // $2 - total disk size
        // $3 - used space
        // $4 - free space
        // $6 - partition mount point (root in below command)
        $cmd = shell_exec('df / | awk \'$6 == "/" {print $2 " " $3 " " $4}\'');
        $values = explode(' ', $cmd);

        $this->totalSpace = trim($values[0]) * 1024;
        $this->usedSpace = trim($values[1]) * 1024;
        $this->freeSpace = trim($values[2]) * 1024;
    }

    public function getTotalSpace()
    {
        return $this->totalSpace;
    }

    public function getFreeSpace()
    {
        return $this->freeSpace;
    }

    public function getUsedSpace()
    {
        return $this->usedSpace;
    }
}
