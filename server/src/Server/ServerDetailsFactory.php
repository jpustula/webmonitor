<?php

namespace Server;

class ServerDetailsFactory
{
    private $serverDetails = [];

    private function setCpuDetails()
    {
        $cpu = new \Server\Details\Cpu();

        $this->serverDetails['cpu'] = [
            'load' => $cpu->getLoad(),
            'cores_number' => $cpu->getCoresNumber(),
        ];
    }

    private function setDiskDetails()
    {
        $disk = new \Server\Details\Disk();

        $this->serverDetails['disk'] = [
            'total_space' => $disk->getTotalSpace(),
            'free_space' => $disk->getFreeSpace(),
            'used_space' => $disk->getUsedSpace(),
        ];
    }

    private function setMemoryDetails()
    {
        $memory = new \Server\Details\Memory();

        $this->serverDetails['memory'] = [
            'size' => $memory->getSize(),
            'free' => $memory->getFree(),
            'used' => $memory->getUsed(),
        ];
    }

    private function setSwapDetails()
    {
        $swap = new \Server\Details\Swap();

        $this->serverDetails['swap'] = [
            'size' => $swap->getSize(),
            'free' => $swap->getFree(),
            'used' => $swap->getUsed(),
        ];
    }

    private function setSystemDetails()
    {
        $system = new \Server\Details\System();

        $this->serverDetails['system'] = [
            'operating_system' => $system->getOperatingSystem(),
            'uptime' => $system->getUptime(),
        ];
    }

    private function setNetworkDetails()
    {
        $network = new \Server\Details\Network();

        $this->serverDetails['network'] = [
            'hostname' => $network->getHostname(),
            'ip' => $network->getIp(),
        ];
    }

    public function getAllDetails()
    {
        $this->setCpuDetails();
        $this->setDiskDetails();
        $this->setMemoryDetails();
        $this->setSwapDetails();
        $this->setSystemDetails();
        $this->setNetworkDetails();

        return $this->serverDetails;
    }
}
