<?php

function get_last_event_id($headers)
{
    if (isset($headers['Last-Event-ID']))
    {
        return $headers['Last-Event-ID'];
    }

    return null;
}

function printMessage($message)
{
    global $config;

    if (isset($config['debug']) && !$config['debug'])
    {
        return;
    }

    echo $message;
}
