<?php

require __DIR__ . '/config.php';
require __DIR__ . '/vendor/autoload.php';

use Clue\React\Sse\BufferedChannel;
use React\Http\Request;
use React\Http\Response;

$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\Server($loop);
$channel = new BufferedChannel();
$http = new React\Http\Server($socket);
$server_details_factory = new \Server\ServerDetailsFactory();

// Handle connecting/disconnecting clients
$http->on('request', function (Request $request, Response $response) use ($channel, $server_details_factory) {
    switch ($request->getPath())
    {
        case '/sse':
            printMessage('Connected SSE client' . PHP_EOL);

            $id = get_last_event_id($request->getHeaders());
            $response->writeHead(200, ['Content-Type' => 'text/event-stream']);
            $channel->connect($response, $id);

            $response->on('close', function () use ($response, $channel) {
                $channel->disconnect($response);
                printMessage('Disconnected SSE client' . PHP_EOL);
            });
        break;

        case '/ajax':
            printMessage('Connected AJAX client' . PHP_EOL);

            $server_details = $server_details_factory->getAllDetails();
            $server_details['time'] = time();

            $response->writeHead('200', ['Content-Type' => 'application/json']);
            $response->end(json_encode($server_details));

            return;
        break;
    }
});

$loop->addPeriodicTimer($config['sse_push_interval'], function() use ($channel, $server_details_factory) {
    $server_details = $server_details_factory->getAllDetails();
    $server_details['time'] = time();
    $channel->writeMessage(json_encode($server_details));
});

$socket->listen(8080, '0.0.0.0');
printMessage('Server listening on localhost:' . $socket->getPort() . PHP_EOL);

$loop->run();
