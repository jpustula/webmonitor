#!/usr/bin/env bash

# Common tasks
./scripts/system.sh
./scripts/ssh.sh
./scripts/git.sh

# Enable CORS header on all nginx responses
sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.backup
awk '/default_type application\/octet-stream;/ { print; print "\n\tadd_header '\''Access-Control-Allow-Origin'\'' '\''*'\'' always;"; next }1' /etc/nginx/nginx.conf > ~/nginx.conf
sudo mv ~/nginx.conf /etc/nginx/nginx.conf

# Add virtual hosts to nginx
sudo cp /srv/config/nginx/s2.webmonitor.local /etc/nginx/sites-available
sudo ln -s /etc/nginx/sites-available/s2.webmonitor.local /etc/nginx/sites-enabled/s2.webmonitor.local

sudo service nginx reload

# Download server dependencies
cd /srv/server
composer install

# Copy run script
cp /srv/config/run.sh /srv/server
