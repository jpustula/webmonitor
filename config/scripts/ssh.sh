#!/usr/bin/env bash

PRIVATE_KEY_FILE=/srv/id_rsa

if [ -f $PRIVATE_KEY_FILE ]
then
	cp $PRIVATE_KEY_FILE ~/.ssh
	chmod 0600 ~/.ssh/id_rsa
fi
