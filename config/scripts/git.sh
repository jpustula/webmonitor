#!/usr/bin/env bash

git config --global user.name "jaroslw"
git config --global user.email "jaroslaw@pustula.com"

# Clone project repository
git clone git@bitbucket.org:jaroslw/webmonitor.git /srv
