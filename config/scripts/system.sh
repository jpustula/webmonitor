#!/usr/bin/env bash

# Update system and install new packages
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get -y install htop stress

# Update tools
sudo composer self-update

# Change system or user settings
sed -i "s/alias la='ls -A'/alias la='ls -lA'/g" ~/.bashrc
