<?php

use Illuminate\Database\Seeder;

class ServersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Server::create([
            'user_id' => 1,
            'hostname' => 's1.webmonitor.local',
            'description' => 'Client application server',
            'sse_uri' => '//s1.webmonitor.local/sse',
            'ajax_uri' => '//s1.webmonitor.local/ajax',
            'enabled' => true
        ]);

        \App\Server::create([
            'user_id' => 1,
            'hostname' => 's2.webmonitor.local',
            'description' => 'Test server',
            'sse_uri' => '//s2.webmonitor.local/sse',
            'ajax_uri' => '//s2.webmonitor.local/ajax',
            'enabled' => true
        ]);

        \App\Server::create([
            'user_id' => 1,
            'hostname' => 's3.webmonitor.local',
            'sse_uri' => '//s3.webmonitor.local/sse',
            'ajax_uri' => '//s3.webmonitor.local/ajax',
            'enabled' => false
        ]);

        \App\Server::create([
            'user_id' => 1,
            'hostname' => 's4.webmonitor.local',
            'sse_uri' => '//s4.webmonitor.local/sse',
            'ajax_uri' => '//s4.webmonitor.local/ajax',
            'enabled' => false
        ]);
    }
}
