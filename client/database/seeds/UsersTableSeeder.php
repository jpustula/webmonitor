<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'username' => 'administrator',
            'email' => '195895@student.pwr.edu.pl',
            'password' => Hash::make('haslo1'),
            'connecting_method' => 'sse',
            'ajax_refresh_interval' => 2000
        ]);

        \App\User::create([
            'username' => 'test1',
            'email' => 'test1@weeb.pl',
            'password' => Hash::make('haslo2'),
            'connecting_method' => 'sse',
            'ajax_refresh_interval' => 2000
        ]);

        \App\User::create([
            'username' => 'test.account',
            'email' => 'test2@weeb.pl',
            'password' => Hash::make('haslo3'),
            'connecting_method' => 'sse',
            'ajax_refresh_interval' => 2000
        ]);
    }
}
