<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function getLogin()
    {
        $data = ['page' => 'login'];

        return view('pages.login', $data);
    }

    public function postLogin(Request $request)
    {
        $credentials = [
            'username' => $request->input('username'),
            'password' => $request->input('password')
        ];

        if (Auth::attempt($credentials))
        {
            return redirect()->intended(route('home'));
        }

        return back()
            ->withInput($request->except('password'))
            ->with('flash_error', 'Incorrect username or password.');
    }

    public function getLogout()
    {
        Auth::logout();

        return redirect()->route('auth.login')
            ->with('flash_notice', 'Successfully logged out.');
    }
}
