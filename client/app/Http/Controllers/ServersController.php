<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $server = [
            'user_id' => auth()->user()->id,
            'hostname' => $request->input('hostname'),
            'description' => (!empty($request->input('description'))) ? $request->input('description') : null,
            'sse_uri' => $request->input('sse_uri'),
            'ajax_uri' => $request->input('ajax_uri'),
            'enabled' => true
        ];

        \App\Server::create($server);

        return back()
            ->with('notify_success', 'Server has been created.');
    }

    public function show($hostname)
    {
        $server = \App\Server::where('hostname', $hostname)->firstOrFail();

        $data = [
            'page' => 'server',
            'server' => $server
        ];

        return view('pages.server', $data);
    }

    public function destroy($id)
    {
        $server = \App\Server::findOrFail($id);
        $server->delete();

        return back()
            ->with('notify_success', 'Server has been deleted.');
    }

    public function update(Request $request, $id)
    {
        $server = \App\Server::findOrFail($id);

        $input = $request->except(['_token', '_method']);

        foreach ($input as $key => $value)
        {
            $data[$key] = $value;
        }

        $server->update($data);

        return back()
            ->with('notify_success', 'Server details has been updated.');
    }
}
