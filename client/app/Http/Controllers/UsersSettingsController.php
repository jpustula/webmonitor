<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = \App\User::all();

        $data = [
            'page' => 'settings',
            'users' => $users
        ];

        return view('pages.users_settings', $data);
    }
}
