<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AccountSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = ['page' => 'settings'];

        return view('pages.account_settings', $data);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'old_password' => 'required_with:new_password|min:6',
            'new_password' => 'required_with:old_password|confirmed|min:6'
        ]);

        $user = auth()->user();

        $input = $request->except([
            '_token',
            '_method',
            'old_password',
            'new_password',
            'new_password_confirmation'
        ]);

        // Update standard fields
        foreach ($input as $key => $value)
        {
            $data[$key] = $value;
        }

        // Handle password change
        if ($request->has('old_password') && $request->has('new_password'))
        {
            $is_old_password_correct = Hash::check($request->input('old_password'), $user->password);

            if (!$is_old_password_correct)
            {
                return back()
                    ->withInput()
                    ->with('flash_error', 'Current password is incorrect.');
            }

            $data['password'] = Hash::make($request->input('new_password'));
        }

        $user->update($data);

        return back()
            ->with('flash_success', 'Changes saved successfully.');
    }
}
