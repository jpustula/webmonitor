<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServersController extends Controller
{
    public function listAll()
    {
        $servers = \App\Server::all();

        return response()->json($servers);
    }

    public function listEnabled()
    {
        $servers = \App\Server::where('enabled', true)->get();

        return response()->json($servers);
    }

    public function show($id)
    {
        $server = \App\Server::findOrFail($id);

        return response()->json($server);
    }
}
