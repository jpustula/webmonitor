<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GeneralSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = ['page' => 'settings'];

        return view('pages.general_settings', $data);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'connecting_method' => 'required|in:sse,ajax',
            'ajax_refresh_interval' => 'required|integer|min:1000|max:60000'
        ]);

        $user = auth()->user();

        $input = $request->except(['_token', '_method']);

        // Update standard fields
        foreach ($input as $key => $value)
        {
            $data[$key] = $value;
        }

        $user->update($data);

        return back()
            ->with('flash_success', 'Changes saved successfully.');
    }
}
