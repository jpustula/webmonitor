<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app = [
    'domain' => getenv('DOMAIN')
];

$api = [
    'domain' => getenv('API_DOMAIN'),
    'namespace' => 'Api'
];

Route::group($app, function () {
    get('/', 'DashboardController@index')->name('home');

    get('login', 'AuthController@getLogin')->name('auth.login');
    post('login', 'AuthController@postLogin');
    get('logout', 'AuthController@getLogout')->name('auth.logout');

    post('servers', 'ServersController@store')->name('servers.store');
    get('servers/{hostname}', 'ServersController@show')->name('servers.show');
    delete('servers/{id}', 'ServersController@destroy')->name('servers.destroy');
    patch('servers/{id}', 'ServersController@update')->name('servers.update');

    get('settings/general', 'GeneralSettingsController@index')->name('settings.general.index');
    patch('settings/general', 'GeneralSettingsController@update')->name('settings.general.update');

    get('settings/account', 'AccountSettingsController@index')->name('settings.account.index');
    patch('settings/account', 'AccountSettingsController@update')->name('settings.account.update');

    get('settings/users', 'UsersSettingsController@index')->name('settings.users.index');
});

Route::group($api, function () {
    get('servers/all', 'ServersController@listAll')->name('api.servers.all');
    get('servers/enabled', 'ServersController@listEnabled')->name('api.servers.enabled');
    get('servers/{id}', 'ServersController@show')->name('api.servers.show');
});
