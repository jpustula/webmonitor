<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole())
        {
            return;
        }

        $all_servers = \App\Server::all();
        $enabled_servers = collect([]);

        foreach ($all_servers as $server)
        {
            if ($server->enabled)
            {
                 $enabled_servers->push($server);
            }
        }

        $data = [
            'all_servers' => $all_servers,
            'enabled_servers' => $enabled_servers,
            'total_servers' => $all_servers->count(),
            'total_enabled_servers' => $enabled_servers->count()
        ];

        view()->share($data);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
