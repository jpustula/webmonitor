var pieCharSize = 180;

var cpuLoadChartPlaceholder = $('#cpu-load-chart');
var cpuMinuteLoadSamplesData = [];
var cpuFiveMinutesLoadSamplesData = [];
var cpuFifteenMinutesLoadSamplesData = [];
var maxCpuLoadSamples = 50;
var updateInterval = 2000;
var now = new Date().getTime() - maxCpuLoadSamples * updateInterval;

var diskUsageChart = c3.generate({
	bindto: '#disk-usage-chart',
	size: { height: pieCharSize },
	tooltip: { show: false },
	legend: { show: false },
	pie: {
		label: { show: false }
	},
	color: {
		pattern: [
			'#ddd',
			'#03a9f4'
		]
	},
	data: {
		type: 'pie',
		columns: [
			['Free space', 1],
			['Used space', 0]
		]
	}
});

var memoryUtilizationChart = c3.generate({
	bindto: '#memory-utilization-chart',
	size: { height: pieCharSize },
	tooltip: { show: false },
	legend: { show: false },
	donut: {
		label: { show: false }
	},
	color: {
		pattern: [
			'#ddd',
			'#e91e63'
		]
	},
	data: {
		type: 'donut',
		columns: [
			['Free memory', 1],
			['Used memory', 0]
		]
	}
});

var swapUtilizationChart = c3.generate({
	bindto: '#swap-utilization-chart',
	size: { height: pieCharSize },
	tooltip: { show: false },
	legend: { show: false },
	donut: {
		label: { show: false }
	},
	color: {
		pattern: [
			'#ddd',
			'#009688'
		]
	},
	data: {
		type: 'donut',
		columns: [
			['Free SWAP', 1],
			['Used SWAP', 0]
		]
	}
});

var cpuLoadChartOptions = {
	series: {
		lines: {
			lineWidth: 2,
			fill: true
		}
	},
	xaxis: {
		mode: 'time',
		tickSize: [2, 'second'],
		tickFormatter: function (x, axis) {
			var date = new Date(x);

			if (date.getSeconds() % 10 == 0) {
				var hours = (date.getHours() < 10) ? '0' + date.getHours() : date.getHours();
				var minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes();
				var seconds = (date.getSeconds() < 10) ? '0' + date.getSeconds() : date.getSeconds();

				return hours + ':' + minutes + ':' + seconds;
			} else {
				return '';
			}
		}
	},
	yaxis: {
		min: 0,
		max: 100,
		tickSize: 10,
		tickFormatter: function (y, axis) {
			if (y % 20 == 0) {
				return y + '%';
			} else {
				return '';
			}
		}
	},
	legend: {
		backgroundOpacity: 0,
		labelBoxBorderColor: "#999"
	},
	grid: {
		backgroundColor: '#fff',
		tickColor: '#ddd',
		borderColor: '#ddd',
		borderWidth: 1
	}
};

var cpuLoadChartDataSets = [
	{ label: '15 min', data: cpuFifteenMinutesLoadSamplesData, color: '#4CAF50'},
	{ label: '5 min', data: cpuFiveMinutesLoadSamplesData, color: '#e91e63'},
	{ label: '1 min', data: cpuMinuteLoadSamplesData, color: '#03a9f4'}
];

setCpuLoadInitSamples();
$.plot(cpuLoadChartPlaceholder, cpuLoadChartDataSets, cpuLoadChartOptions);

function updateCharts(server) {
	updateDiskUsageChart(server);
	updateMemoryUtilizationChart(server);
	updateSwapUtilizationChart(server);
	updateCpuLoadChart(server);
}

function updateDiskUsageChart(server) {
	if (server.status == STATUS_RUNNING) {
		var diskSize = server.details.disk.total_space;
		var freeSpace = server.details.disk.free_space;
		var usedSpace = server.details.disk.used_space;

		var freeSpacePercentage = Math.round(freeSpace / diskSize * 100);
		var usedSpacePercentage = Math.round(usedSpace / diskSize * 100);

		$('.js-disk-free-space').html(freeSpacePercentage + '%');
		$('.js-disk-used-space').html(usedSpacePercentage + '%');

		diskUsageChart.load({
			columns: [
				['Free space', freeSpace],
				['Used space', usedSpace]
			]
		});
	} else {
		$('.js-disk-free-space').html('no usage data');
		$('.js-disk-used-space').html('no usage data');

		diskUsageChart.load({
			columns: [
				['Free space', 1],
				['Used space', 0]
			]
		});
	}
}

function updateMemoryUtilizationChart(server) {
	if (server.status == STATUS_RUNNING) {
		var totalMemory = server.details.memory.size;
		var freeMemory = server.details.memory.free;
		var usedMemory = server.details.memory.used;

		var freeMemoryPercentage = Math.round(freeMemory / totalMemory * 100);
		var usedMemoryPercentage = Math.round(usedMemory / totalMemory * 100);

		$('.js-free-memory').html(freeMemoryPercentage + '%');
		$('.js-used-memory').html(usedMemoryPercentage + '%');

		memoryUtilizationChart.load({
			columns: [
				['Free memory', freeMemory],
				['Used memory', usedMemory]
			]
		});
	} else {
		$('.js-free-memory').html('no usage data');
		$('.js-used-memory').html('no usage data');

		memoryUtilizationChart.load({
			columns: [
				['Free memory', 1],
				['Used memory', 0]
			]
		});
	}
}

function updateSwapUtilizationChart(server) {
	if (server.status == STATUS_RUNNING) {
		var totalSwap = server.details.swap.size;
		var freeSwap = server.details.swap.free;
		var usedSwap = server.details.swap.used;

		var freeSwapPercentage = Math.round(freeSwap / totalSwap * 100);
		var usedSwapPercentage = Math.round(usedSwap / totalSwap * 100);

		$('.js-free-swap').html(freeSwapPercentage + '%');
		$('.js-used-swap').html(usedSwapPercentage + '%');

		swapUtilizationChart.load({
			columns: [
				['Free SWAP', freeSwap],
				['Used SWAP', usedSwap]
			]
		});
	} else {
		$('.js-free-swap').html('no usage data');
		$('.js-used-swap').html('no usage data');

		swapUtilizationChart.load({
			columns: [
				['Free SWAP', 1],
				['Used SWAP', 0]
			]
		});
	}
}

function setCpuLoadInitSamples() {
	while (cpuMinuteLoadSamplesData.length < maxCpuLoadSamples) {
		var x = now += updateInterval;
		var y = 0;
		var sample = [x, y];

		cpuMinuteLoadSamplesData.push(sample);
		cpuFiveMinutesLoadSamplesData.push(sample);
		cpuFifteenMinutesLoadSamplesData.push(sample);
	}
}

function updateCpuLoadChart(server) {
	var yMinute;
	var yFiveMinutes;
	var yFiveteenMinutes;

	// Remove the oldest data sample
	cpuMinuteLoadSamplesData.shift();
	cpuFiveMinutesLoadSamplesData.shift();
	cpuFifteenMinutesLoadSamplesData.shift();

	var x = now += updateInterval;

	if (server.status == STATUS_RUNNING) {
		yMinute = ((server.details.cpu.load[0] * 100) < 100) ? server.details.cpu.load[0] * 100 : 100;
		yFiveMinutes = ((server.details.cpu.load[1] * 100) < 100) ? server.details.cpu.load[1] * 100 : 100;
		yFiveteenMinutes = ((server.details.cpu.load[2] * 100) < 100) ? server.details.cpu.load[2] * 100 : 100;
	} else {
		yMinute = 0;
		yFiveMinutes = 0;
		yFiveteenMinutes = 0;
	}

	// Add new data sample to set
	cpuMinuteLoadSamplesData.push([x, yMinute]);
	cpuFiveMinutesLoadSamplesData.push([x, yFiveMinutes]);
	cpuFifteenMinutesLoadSamplesData.push([x, yFiveteenMinutes]);

	// Redraw graph
	$.plot(cpuLoadChartPlaceholder, cpuLoadChartDataSets, cpuLoadChartOptions);
}
