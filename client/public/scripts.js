const STATUS_DISCONNECTED = 0;
const STATUS_RUNNING = 1;

function setRunningLabel(selector) {
	$(selector).html('<span class="label label-success">Running</span>');
}

function setDisconnectedLabel(selector) {
	$(selector).html('<span class="label label-danger">Disconnected</span>');
}

function updateServersOverview(servers) {
	var totalServers = servers.length;
	var totalRunningServers = 0;
	var totalDisconnectedServers;
	var runningServersPercentage = 0;
	var disconnectedServersPercentage = 0;

	servers.forEach(function (server, i) {
		var serverRow = $('[data-server-id="' + server.id + '"');
		var serverUptime = $(serverRow).children('.js-uptime');
		var serverStatus = $(serverRow).children('.js-status');

		if (server.status == STATUS_RUNNING) {
			totalRunningServers++;
			serverUptime.html(server.details.system.uptime);
			setRunningLabel(serverStatus);
		} else {
			serverUptime.html('No data');
			setDisconnectedLabel(serverStatus);
		}
	});

	totalDisconnectedServers = totalServers - totalRunningServers;

	if (totalServers > 0) {
		runningServersPercentage = Math.round(totalRunningServers / totalServers * 100);
		disconnectedServersPercentage = 100 - runningServersPercentage;
	}

	$('.js-running-servers').html(totalRunningServers + ' (' + runningServersPercentage + '%)');
	$('.js-disconnected-servers').html(totalDisconnectedServers + ' (' + disconnectedServersPercentage + '%)');
}

function updateServerDetails(server) {
	var serverStatus = $('.js-server-status');

	if (server.status == STATUS_RUNNING) {
		setRunningLabel(serverStatus);
		$('.js-server-uptime').html(server.details.system.uptime);
		$('.js-server-system').html(server.details.system.operating_system);
		$('.js-server-ip').html(server.details.network.ip);
		$('.js-server-hostname').html(server.details.network.hostname);
		$('.js-server-cpu-cores').html(server.details.cpu.cores_number);
		$('.js-server-disk-size').html(formatBytes(server.details.disk.total_space));
		$('.js-server-ram-size').html(formatBytes(server.details.memory.size));
		$('.js-server-swap-size').html(formatBytes(server.details.swap.size));
	} else {
		setDisconnectedLabel(serverStatus);
		$('.js-server-uptime').html('No data');
		$('.js-server-system').html('No data');
		$('.js-server-ip').html('No data');
		$('.js-server-hostname').html('No data');
		$('.js-server-cpu-cores').html('No data');
		$('.js-server-disk-size').html('No data');
		$('.js-server-ram-size').html('No data');
		$('.js-server-swap-size').html('No data');
		$('#data-console').text('');
	}
}

function confirmServerDelete() {
	var deleteButton = $('.js-delete-server');
	var confirmationModal = $('#delete-serve-modal');

	$(deleteButton).click(function () {
		var serverId = $(this).closest('tr').data('server-id');
		var serverHostname = $(this).closest('tr').data('server-hostname');

		$(confirmationModal).find('.js-hostname').html(serverHostname);
		$(confirmationModal).find('form').attr('action', '/servers/' + serverId);

		$(confirmationModal).modal();
	});
}

function showServerEditModal(resourceUrl) {
	var editButton = $('.js-edit-server');
	var editModal = $('#edit-server-modal');

	$(editButton).click(function () {
		var serverId = $(this).closest('tr').data('server-id');

		$.getJSON(resourceUrl + '/' + serverId, function(data) {
			var server = data;

			$(editModal).find('form').attr('action', '/servers/' + serverId);
			$(editModal).find('[name="hostname"]').val(server.hostname);
			$(editModal).find('[name="sse_uri"]').val(server.sse_uri);
			$(editModal).find('[name="ajax_uri"]').val(server.ajax_uri);
			$(editModal).find('[name="description"]').val(server.description);

			$(editModal).modal();
		});
	});
}

function formatBytes(bytes) {
	if (bytes == 0) {
		return '0 bytes';
	}

	var sizes = ['bytes', 'KB', 'MB', 'GB', 'TB'];
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));

	return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
}
