@extends('templates.settings')

@section('meta-title', 'Users')

@section('settings.content')
	<div class="col-md-9">
		<div class="container-detached">
			<div class="content-detached">

				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Users</h5>
					</div>

					<div class="panel-body">
						List of all accounts registered in the application.
					</div>

					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Username</th>
									<th>E-mail address</th>
									<th>Register date</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($users as $i => $user)
									<tr>
										<td>{{ ++$i }}</td>
										<td>{{ $user->username }}</td>
										<td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
										<td>{{ $user->created_at->format('Y-m-d, H:i') }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection
