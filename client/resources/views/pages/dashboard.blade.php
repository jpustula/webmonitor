@extends('templates.master')

@section('meta-title', 'Dashboard')

@section('content')
	<div class="row">
		<div class="col-md-3 col-sm-6">
			<div class="panel bg-blue-400">
				<div class="panel-body">
					<h3 class="no-margin">{{ $total_enabled_servers }}</h3>
					<span class="text-uppercase">Actively monitored servers</span>
				</div>
			</div>
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="panel bg-success-400">
				<div class="panel-body">
					<h3 class="no-margin js-running-servers">No data</h3>
					<span class="text-uppercase">Operating normally</span>
				</div>

				<div id="server-load"></div>
			</div>
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="panel bg-danger-400">
				<div class="panel-body">
					<h3 class="no-margin js-disconnected-servers">No data</h3>
					<span class="text-uppercase">Servers has outage</span>
				</div>

				<div id="today-revenue"></div>
			</div>
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="panel bg-orange-300">
				<div class="panel-body">
					<h3 class="no-margin">{{ $total_servers }}</h3>
					<span class="text-uppercase">Total servers in database</span>
				</div>

				<div id="today-revenue"></div>
			</div>
		</div>
	</div>

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Overview</h5>
		</div>

		<div class="panel-body">
			A shortened list of servers monitored by the system.
		</div>

		<table class="table table-striped text-nowrap">
			<thead>
				<tr>
					<th>Server</th>
					<th class="col-md-4">Description</th>
					<th class="col-md-2">Uptime</th>
					<th colspan="2" class="col-md-3">Status</th>
				</tr>
			</thead>
			<tbody>
				@forelse ($all_servers as $server)
					<tr data-server-id="{{ $server->id }}" data-server-hostname="{{ $server->hostname }}">
						<td>
							@if ($server->enabled)
								<a href="{{ route('servers.show', [$server->hostname]) }}">{{ $server->hostname }}</a>
							@else
								<span class="text-muted">{{ $server->hostname }}</span>
							@endif
						</td>
						<td>
							<span class="text-muted">{{ $server->description or '-' }}</span>
						</td>
						<td class="js-uptime">No data</td>
						<td class="js-status">
							<span class="label label-default">Disabled</span>
						</td>
						<td class="text-right">
							<ul class="icons-list">
								<li>
									<form action="{{ route('servers.update', [$server->id]) }}" method="post">
										{!! csrf_field() !!}
										{!! method_field('patch') !!}

										@if ($server->enabled)
											<input type="hidden" name="enabled" value="0">

											<button type="submit" class="btn btn-link text-success-700" title="Disable monitoring">
												<i class="icon-switch"></i>
											</button>
										@else
											<input type="hidden" name="enabled" value="1">

											<button type="submit" class="btn btn-link text-grey-300" title="Enable monitoring">
												<i class="icon-switch"></i>
											</button>
										@endif
									</form>
								</li>
								<li>
									<button type="button" class="btn btn-link text-primary js-edit-server" title="Edit">
										<i class="icon-pencil7"></i>
									</button>
								</li>
								<li>
									<button type="button" class="btn btn-link text-danger js-delete-server" title="Delete">
										<i class="icon-trash"></i>
									</button>
								</li>
							</ul>
						</td>
					</tr>
				@empty
					<tr>
						<td colspan="5" class="text-center">No servers added. Create a new one.</td>
					</tr>
				@endforelse
			</tbody>
		</table>
	</div>

	@include('templates.server_delete')
	@include('templates.server_edit')
@endsection

@section('footer-js')
	@parent

	<script>
		$.getJSON('{{ route('api.servers.enabled') }}', function(data) {
			var connectingMethod = '{{ auth()->user()->connecting_method }}';
			var servers = data;

			switch (connectingMethod) {
				case 'sse':

					var eventSources = [];

					// Connect to each server using Server-Sent Events
					servers.forEach(function (server, i) {
						var uri = server.sse_uri;
						var eventSource = eventSources[i];

						eventSource = new EventSource(uri);

						eventSource.addEventListener('open', function () {
							server.status = STATUS_RUNNING;
						}, false);

						eventSource.addEventListener('error', function () {
							server.status = STATUS_DISCONNECTED;
						}, false);

						eventSource.addEventListener('message', function (e) {
							server.details = JSON.parse(e.data);
						}, false);

						// Redraw HTML of servers overview
						['open', 'error', 'message'].forEach(function (e) {
							eventSource.addEventListener(e, function() {
								updateServersOverview(servers);
							});
						});
					});

					break;

				case 'ajax':

					// Connect to each server using Server-Sent Events
					servers.forEach(function (server, i) {
						var uri = server.ajax_uri;

						ajax(servers, server, uri);
					});

					break;
			}
		});

		confirmServerDelete();
		showServerEditModal('{{ route('api.servers.show', ['']) }}');

		function ajax(servers, server, uri) {
			$.getJSON(uri, function (data) {
				server.details = data;
			})
			.done(function () {
				server.status = STATUS_RUNNING;
			})
			.fail(function () {
				server.status = STATUS_DISCONNECTED;
			})
			.always(function () {
				updateServersOverview(servers);
				setTimeout(ajax, '{{ auth()->user()->ajax_refresh_interval }}', servers, server, uri);
			});
		}
	</script>
@endsection
