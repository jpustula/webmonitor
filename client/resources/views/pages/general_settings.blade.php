@extends('templates.settings')

@section('meta-title', 'General settings')

@section('settings.content')
	<div class="col-md-9">
		<div class="container-detached">
			<div class="content-detached">

				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">General settings</h5>
					</div>

					<div class="panel-body">
						@include('templates.alerts')

						<form action="{{ route('settings.general.update') }}" method="post" class="form-horizontal">
							{!! csrf_field() !!}
							{!! method_field('patch') !!}

							<fieldset class="content-group">
								<div class="form-group">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<label class="control-label col-sm-4 text-right">Connecting method:</label>
											<div class="col-sm-8">
												<select name="connecting_method" class="form-control">
													<option value="sse" @if (auth()->user()->isConnectingMethod('sse')) selected @endif>HTML5 SSE (Server-Sent Events)</option>
													<option value="ajax" @if (auth()->user()->isConnectingMethod('ajax')) selected @endif>AJAX</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<label class="control-label col-sm-4 text-right">AJAX refresh interval:</label>
											<div class="col-sm-8">
												<div class="input-group">
													<input type="text" name="ajax_refresh_interval" class="form-control" value="{{ auth()->user()->ajax_refresh_interval }}">
													<span class="input-group-addon">ms</span>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group" style="margin-top: 40px;">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<div class="col-sm-8 col-sm-offset-4">
												<button type="submit" class="btn btn-primary">Save changes</button>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection
