@extends('templates.master')

@section('meta-title', 'Login')

@section('container')
	<div class="page-container login-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">

					<div class="panel panel-body login-form">
						<div class="text-center">
							<div class="icon-object border-slate-300 text-slate-300"><i class="icon-user-lock"></i></div>
							<h5 class="content-group">
								Login to your account
								<small class="display-block">Enter credentials below</small>
							</h5>
						</div>

						@include('templates.alerts')

						<form action="{{ route('auth.login') }}" method="post">
							{!! csrf_field() !!}

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" name="username" class="form-control" value="{{ old('username') }}" placeholder="Username" autofocus>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" name="password" class="form-control" placeholder="Password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group" style="margin-bottom: 9px;">
								<button type="submit" class="btn btn-primary btn-block">
									Login <i class="icon-arrow-right14 position-right"></i>
								</button>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection
