@extends('templates.master')

@section('meta-title', $server->hostname . ' • Servers')

@section('content')
	<div class="row">
		<div class="col-md-3">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h5 class="panel-title">
						Monitored server

						<span class="pull-right js-server-status">
							<span class="label label-default">Disabled</span>
						</span>
					</h5>
				</div>

				<div class="panel-body">
					<h6 class="no-margin text-semibold">
						<a href="{{ route('servers.show', [$server->hostname]) }}" style="color: #333;">
							{{ $server->hostname }}
						</a>
					</h6>
					{{ $server->description }}
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h5 class="panel-title">System and hardware</h5>
				</div>

				<table class="table table-xs table-striped">
					<tbody>
						<tr>
							<td class="col-md-5"><strong>Uptime:</strong></td>
							<td class="js-server-uptime">No data</td>
						</tr>
						<tr>
							<td><strong>Hostname:</strong></td>
							<td class="js-server-hostname">No data</td>
						</tr>
						<tr>
							<td><strong>IP address:</strong></td>
							<td class="js-server-ip">No data</td>
						</tr>
						<tr>
							<td><strong>System:</strong></td>
							<td class="js-server-system">No data</td>
						</tr>
						<tr>
							<td><strong>CPU cores:</strong></td>
							<td class="js-server-cpu-cores">No data</td>
						</tr>
						<tr>
							<td><strong>Disk size:</strong></td>
							<td class="js-server-disk-size">No data</td>
						</tr>
						<tr>
							<td><strong>RAM:</strong></td>
							<td class="js-server-ram-size">No data</td>
						</tr>
						<tr>
							<td><strong>SWAP size:</strong></td>
							<td class="js-server-swap-size">No data</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="panel panel-default panel-collapsed">
				<div class="panel-heading">
					<h6 class="panel-title">Data console</h6>
					<div class="heading-elements">
						<ul class="icons-list">
							<li><a data-action="collapse" data-popup="tooltip" title="Collapse"></a></li>
						</ul>
					</div>
				</div>

				<div class="panel-body">
					<textarea id="data-console" class="form-control" readonly></textarea>
				</div>
			</div>

		</div>
		<div class="col-md-9">

			<div class="row">
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title">Disk usage</h5>
						</div>

						<div class="panel-body">
							<div class="chart-container text-center">
								<div id="disk-usage-chart" class="display-inline-block"></div>
							</div>
							<div class="text-center">
								<ul class="chart-legend">
									<li>
										<span class="legend-color" style="background: #03a9f4;"></span>
										Used space
										(<span class="js-disk-used-space">no usage data</span>)
									</li>
									<li>
										<span class="legend-color" style="background: #ddd;"></span>
										Free space
										(<span class="js-disk-free-space">no usage data</span>)
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title">RAM utilization</h5>
						</div>

						<div class="panel-body">
							<div class="chart-container text-center">
								<div id="memory-utilization-chart" class="display-inline-block"></div>
							</div>
							<div class="text-center">
								<ul class="chart-legend">
									<li>
										<span class="legend-color" style="background: #e91e63;"></span>
										Used memory
										(<span class="js-used-memory">no usage data</span>)
									</li>
									<li>
										<span class="legend-color" style="background: #ddd;"></span>
										Free memory
										(<span class="js-free-memory">no usage data</span>)
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title">SWAP utilization</h5>
						</div>

						<div class="panel-body">
							<div class="chart-container text-center">
								<div id="swap-utilization-chart" class="display-inline-block"></div>
							</div>
							<div class="text-center">
								<ul class="chart-legend">
									<li>
										<span class="legend-color" style="background: #009688;"></span>
										Used SWAP
										(<span class="js-used-swap">no usage data</span>)
									</li>
									<li>
										<span class="legend-color" style="background: #ddd;"></span>
										Free SWAP
										(<span class="js-free-swap">no usage data</span>)
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title">CPU average load</h5>
						</div>

						<div class="panel-body">
							<div class="chart-container text-center">
								<div id="cpu-load-chart" style="height: 200px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer-js')
	@parent

	<script src="{{ asset('assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
	<script src="{{ asset('assets/js/plugins/visualization/c3/c3.min.js') }}"></script>
	<script src="{{ asset('assets/js/plugins/visualization/flot/jquery.flot.min.js') }}"></script>
	<script src="{{ asset('assets/js/plugins/visualization/flot/jquery.flot.time.min.js') }}"></script>
	<script src="{{ asset('charts.js') }}"></script>

	<script>
		$.getJSON('{{ route('api.servers.show', [$server->id]) }}', function(data) {
			var connectingMethod = '{{ auth()->user()->connecting_method }}';
			var server = data;
			var consoleLinesCount = 0;
			var printLines = 25;

			switch (connectingMethod) {
				case 'sse':

					var uri = server.sse_uri;
					var eventSource;

					eventSource = new EventSource(uri);

					eventSource.addEventListener('open', function () {
						server.status = STATUS_RUNNING;
					}, false);

					eventSource.addEventListener('error', function () {
						server.status = STATUS_DISCONNECTED;
					}, false);

					eventSource.addEventListener('message', function (e) {
						server.details = JSON.parse(e.data);

						$('#data-console').prepend(e.data + '\n');
						consoleLinesCount++;

						if (consoleLinesCount > printLines) {
							$('#data-console').text('');
							consoleLinesCount = 0;
						}
					}, false);

					// Redraw HTML of servers overview
					['open', 'error', 'message'].forEach(function (e) {
						eventSource.addEventListener(e, function () {
							updateServerDetails(server);
							updateCharts(server);
						});
					});

					break;

				case 'ajax':

					var uri = server.ajax_uri;

					ajax(server, uri);

					break;
			}
		});

		function ajax(server, uri, consoleLinesCount, printLines) {
			$.getJSON(uri, function (data) {
				server.details = data;

				$('#data-console').prepend(JSON.stringify(data) + '\n');
				consoleLinesCount++;

				if (consoleLinesCount > printLines) {
					$('#data-console').text('');
					consoleLinesCount = 0;
				}
			})
			.done(function () {
				server.status = STATUS_RUNNING;
			})
			.fail(function () {
				server.status = STATUS_DISCONNECTED;
			})
			.always(function () {
				updateServerDetails(server);
				updateCharts(server);
				setTimeout(ajax, '{{ auth()->user()->ajax_refresh_interval }}', server, uri, consoleLinesCount, printLines);
			});
		}
	</script>
@endsection
