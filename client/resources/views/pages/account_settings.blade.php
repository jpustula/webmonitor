@extends('templates.settings')

@section('meta-title', 'Account settings')

@section('settings.content')
	<div class="col-md-9">
		<div class="container-detached">
			<div class="content-detached">

				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Account settings</h5>
					</div>

					<div class="panel-body">
						@include('templates.alerts')

						<form action="{{ route('settings.account.update') }}" method="post" class="form-horizontal">
							{!! csrf_field() !!}
							{!! method_field('patch') !!}

							<fieldset class="content-group">
								<legend class="text-bold">Profile</legend>

								<div class="form-group">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<label class="control-label col-sm-4 text-right">Username:</label>
											<div class="col-sm-8">
												<p class="form-control-static">{{ auth()->user()->username }}</p>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<label class="control-label col-sm-4 text-right">E-mail address:</label>
											<div class="col-sm-8">
												<input type="text" name="email" class="form-control" value="{{ auth()->user()->email }}">
											</div>
										</div>
									</div>
								</div>
							</fieldset>

							<fieldset class="content-group">
								<legend class="text-bold">Security</legend>
								<p style="margin-bottom: 25px;">Fill below fields only if you want to change your current password.</p>

								<div class="form-group">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<label class="control-label col-sm-4 text-right">Current password:</label>
											<div class="col-sm-8">
												<input type="password" name="old_password" class="form-control">
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<label class="control-label col-sm-4 text-right">New password:</label>
											<div class="col-sm-8">
												<input type="password" name="new_password" class="form-control">
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<label class="control-label col-sm-4 text-right">Confirm new password:</label>
											<div class="col-sm-8">
												<input type="password" name="new_password_confirmation" class="form-control">
											</div>
										</div>
									</div>
								</div>

								<div class="form-group" style="margin-top: 40px;">
									<div class="col-sm-7 col-sm-offset-2">
										<div class="row">
											<div class="col-sm-8 col-sm-offset-4">
												<button type="submit" class="btn btn-primary">Save changes</button>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection
