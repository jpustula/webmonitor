<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>@yield('meta-title') • webMonitor</title>

	@section('css')
		<link href="//fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet">
		<link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/components.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet">
		<link href="{{ asset('stylesheet.css') }}" rel="stylesheet">
	@show
</head>
<body @if (auth()->check()) class="navbar-top" @endif>

@include('templates.navbar')

@section('container')
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">
					@yield('content')
				</div>
			</div>
		</div>
	</div>

	@include('templates.server_create')
@show

@section('footer-js')
	<script src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
	<script src="{{ asset('assets/js/core/app.js') }}"></script>
	<script src="{{ asset('assets/js/pages/components_notifications_other.js') }}"></script>
	<script src="{{ asset('scripts.js') }}"></script>

	@include('templates.notifications')
@show

</body>
</html>
