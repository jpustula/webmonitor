@extends('templates.master')

@section('content')
	<div class="row">
		<div class="col-md-3">
			<div class="sidebar-detached">
				<div class="sidebar sidebar-default">
					<div class="sidebar-content">

						<div class="sidebar-category">
							<div class="category-title">
								<span>Settings</span>
							</div>

							<div class="category-content no-padding">
								<ul class="navigation navigation-alt navigation-accordion">
									<li>
										<a href="{{ route('settings.general.index') }}">
											<i class="icon-gear"></i> General
										</a>
									</li>
									<li>
										<a href="{{ route('settings.account.index') }}">
											<i class="icon-user"></i> Account
										</a>
									</li>
									<li>
										<a href="{{ route('settings.users.index') }}">
											<i class="icon-users"></i> Users
										</a>
									</li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		@yield('settings.content')
	</div>
@endsection
