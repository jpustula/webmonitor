<div id="edit-server-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary-400">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Edit server details</h6>
			</div>

			<form action="{{ route('servers.update') }}" method="post">
				{!! csrf_field() !!}
				{!! method_field('patch') !!}

				<div class="modal-body">
					<div class="form-group">
						<label>Hostname:</label>
						<input type="text" name="hostname" class="form-control" placeholder="domain.tld" autofocus>
					</div>

					<div class="form-group">
						<label>SSE source URI:</label>
						<input type="text" name="sse_uri" class="form-control" placeholder="//domain.tld/sse">
					</div>

					<div class="form-group">
						<label>AJAX source URI:</label>
						<input type="text" name="ajax_uri" class="form-control" placeholder="//domain.tld/ajax">
					</div>

					<div class="form-group">
						<label>Description:</label>
						<input type="text" name="description" class="form-control">
					</div>
				</div>

				<div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
