@if (auth()->check())
	<div class="navbar navbar-fixed-top navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ route('home') }}" title="Go to dashboard">
				<img src="{{ asset('assets/images/logo_light.png') }}" alt="">
			</a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li>
					<a data-toggle="collapse" data-target="#navbar-mobile" class="collapsed">
						<i class="icon-menu7"></i>
					</a>
				</li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav navbar-right">
				<li @if ($page == 'dashboard') class="active" @endif>
					<a href="{{ route('home') }}">
						<i class="icon-home2 position-left"></i> Dashboard
					</a>
				</li>
				<li class="dropdown @if ($page == 'server') active @endif">
					<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
						<i class="icon-stack3 position-left"></i> Servers <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						@foreach ($enabled_servers as $server)
							<li>
								<a href="{{ route('servers.show', [$server->hostname]) }}">
									<i class="icon-server"></i> {{ $server->hostname }}
								</a>
							</li>
						@endforeach

						@if ($enabled_servers->count() > 0)
							<li class="divider"></li>
						@endif

						<li>
							<a data-toggle="modal" data-target="#add-server-modal">
								<i class="icon-plus2"></i> Add server
							</a>
						</li>
					</ul>
				</li>
				<li @if ($page == 'settings') class="active" @endif>
					<a href="{{ route('settings.general.index') }}">
						<i class="icon-cog5 position-left"></i> Settings
					</a>
				</li>
				<li>
					<a href="{{ route('auth.logout') }}" title="Logout ({{ auth()->user()->username }})">
						<i class="icon-switch2 position-left"></i> Logout
					</a>
				</li>
			</ul>
		</div>
	</div>
@endif
