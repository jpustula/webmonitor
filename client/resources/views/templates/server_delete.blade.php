<div id="delete-serve-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger-400">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Delete server from monitoring system</h6>
			</div>

			<form action="#" method="post">
				{!! csrf_field() !!}
				{!! method_field('delete') !!}

				<div class="modal-body">
					Are you sure you want to delete <strong class="js-hostname">domain.tld</strong> server?
				</div>

				<div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Yes, delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
