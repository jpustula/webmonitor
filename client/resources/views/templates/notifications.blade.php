@if (session()->has('notify_success'))
	<script>
		$.jGrowl('{{ session()->get('notify_success') }}', {
			header: 'Success!',
			theme: 'bg-success',
			position: 'bottom-right'
		});
	</script>
@endif

@if (session()->has('notify_notice'))
	<script>
		$.jGrowl('{{ session()->get('notify_notice') }}', {
			header: 'Notice!',
			theme: 'bg-primary',
			position: 'bottom-right'
		});
	</script>
@endif

@if (session()->has('notify_error'))
	<script>
		$.jGrowl('{{ session()->get('notify_error') }}', {
			header: 'Error!',
			theme: 'bg-danger',
			position: 'bottom-right'
		});
	</script>
@endif
