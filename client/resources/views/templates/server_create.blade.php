<div id="add-server-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-success-400">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Add server to monitoring system</h6>
			</div>

			<form action="{{ route('servers.store') }}" method="post">
				{!! csrf_field() !!}

				<div class="modal-body">
					<div class="form-group">
						<label>Hostname:</label>
						<input type="text" name="hostname" class="form-control" placeholder="domain.tld" autofocus>
					</div>

					<div class="form-group">
						<label>SSE source URI:</label>
						<input type="text" name="sse_uri" class="form-control" placeholder="//domain.tld/sse">
					</div>

					<div class="form-group">
						<label>AJAX source URI:</label>
						<input type="text" name="ajax_uri" class="form-control" placeholder="//domain.tld/ajax">
					</div>

					<div class="form-group">
						<label>Description:</label>
						<input type="text" name="description" class="form-control">
					</div>
				</div>

				<div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success">Add server</button>
				</div>
			</form>
		</div>
	</div>
</div>

@section('footer-js')
	@parent

	<script>
		var createModal = $('#add-server-modal');
		var hostnameInput = $(createModal).find('[name="hostname"]');
		var sseUriInput = $(createModal).find('[name="sse_uri"]');
		var ajaxUriInput = $(createModal).find('[name="ajax_uri"]');

		hostnameInput.focusout(function() {
			var hostname = $(this).val();

			if (hostname) {
				sseUriInput.val('//' + hostname + '/sse');
				ajaxUriInput.val('//' + hostname + '/ajax');
			}
		});
	</script>
@endsection
