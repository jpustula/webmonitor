@if (session()->has('flash_success'))
	<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
		{{ session()->get('flash_success') }}
	</div>
@endif

@if (session()->has('flash_notice'))
	<div class="alert alert-info alert-styled-left alert-arrow-left alert-bordered">
		{{ session()->get('flash_notice') }}
	</div>
@endif

@if (session()->has('flash_error'))
	<div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
		{{ session()->get('flash_error') }}
	</div>
@endif

@if (count($errors) > 0)
	<div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
		<strong>Please correct the following errors:</strong>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
